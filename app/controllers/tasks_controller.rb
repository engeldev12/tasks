class TasksController < ApplicationController

	before_action :set_task, only:[:show, :done, :destroy]
	before_action :authenticate_user!

	def index
		@tasks = Task.all_desc_order_of current_user
	end

	def new
		@task = Task.new
		respond_to do |format|
			format.html
			format.js
		end	
	end

	def show
	
	end	

	def create
		@task = Task.new task_params
		@task.user = current_user 
		
		if @task.save
			msj = "La tarea #{@task.name} ha sido añadida a tu lista de tareas!"
			redirect_to tasks_path, notice: msj
		else
			render :new
		end

	end

	def done

		if @task.done
			msj = "Haz terminado la tarea #{@task.name}!"
			redirect_to tasks_path, notice: msj
		else
			render :show
		end

	end

	def destroy
		
		if @task.destroy
			msj = "La tarea #{@task.name} ha sido eliminada."
			redirect_to tasks_path, notice: msj	
		end	
	
	end	

	private
	def task_params
		params.require(:task).permit(:name, :user)
	end

	def set_task
		@task = Task.find(params[:id])
	end	

end
