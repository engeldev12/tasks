module TasksHelper

	def show_status(task)
		data = {text: "pendiente", clase: "badge-warning"}
		
		if task.is_done
			data = {text: "terminada", clase: "badge-success"}
			return data
		end	

		data
	end

	def show_date(task, fecha_actual=DateTime.now)
		if es_de_hoy(task, fecha_actual)
			return 'Hoy'
		else
			return task.created.strftime('%a, %d %B %Y')
		end	
	end

	private
	def es_de_hoy(task, fecha_actual)
		Date.parse(task.created.to_s).eql?(Date.parse(fecha_actual.to_s))
	end

end