class Task < ApplicationRecord

	validates :name, presence: { message: "El nombre es obligatorio!"} 
	belongs_to :user
	scope :all_desc_order_of, -> (user) { where(user: user).order(created_at: :desc) }
	scope :dones_of, -> (user) { where(is_done: true, user: user) }
	scope :undones_of, -> (user) { where(is_done: false, user: user) } 

	before_create :set_created

	def done
		unless self.is_done
			self.is_done = true
			return self.save
		end

		self.errors[:is_done] << "Esta tarea ya fue terminada!"
		return false
	end

	private
	def set_created
		self.created = DateTime.now
	end	

end