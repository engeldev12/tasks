Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	resources :tasks, only: [:new, :create, :index, :show, :destroy]
	
	root 'tasks#index' 

	put 'task/:id/done', to: 'tasks#done', as: 'task_done'
  
end
