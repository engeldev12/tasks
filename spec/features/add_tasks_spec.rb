require 'rails_helper'

RSpec.feature "Agregar tareas", type: :feature do

	context "cuando los datos de una tarea son inválidos" do
		
		scenario "dará error cuando no ingrese el nombre" do
			
			sign_in create(:user)

			visit new_task_path

			fill_in "task_name", with: ""

			find("#btn-guardar").click 

			expect(page).to have_content("El nombre es obligatorio!")
				
		end
	
	end

	context "cuando el usuario no esté logueado" do

		scenario "deberá loguearse para crear una nueva tarea" do
			
			visit new_task_path

			expect(page).to have_content("Log in")
		
		end	

	end	

	context "cuando los datos de una tarea son correctos" do

		scenario "al crearla irá a mi lista de tareas" do
			
			sign_in create(:user)

			visit new_task_path

			fill_in "task_name", with: "Programar"
			
			find("#btn-guardar").click 

			expect(page).to have_content("La tarea Programar ha sido añadida a tu lista de tareas!")
		end

	end	

end
