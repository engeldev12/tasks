require 'rails_helper'

RSpec.feature "Crear usuarios", type: :feature do
	
	context "cuando cree mi usuario y me loguee" do

		scenario "al visitar mis tareas, no tendré tareas creadas" do
			
			sign_in create(:user)
			
			visit tasks_path

			expect(page).to have_content("No tienes tareas creadas!")

		end

	end

end
