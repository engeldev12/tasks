require 'rails_helper'

RSpec.feature "Eliminar tareas", type: :feature do
  
  before(:each) do
  	@user = create(:user)
  end	
	
	context 'cuando se loguee el usuario' do

		scenario 'podrá eliminar sus tareas' do
			
			sign_in @user
			create(:task, user: @user)
			
			visit tasks_path

			within('#todas') do

				click_on('Eliminar')
			
			end	

			expect(page).to have_content('La tarea Jugar play ha sido eliminada.')		

		end	

	end	

end
