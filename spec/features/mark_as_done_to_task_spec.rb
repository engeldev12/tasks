require 'rails_helper'

RSpec.feature "Marcar tareas como terminadas", type: :feature do

	before(:each) do
		sign_in create(:user)
		crear_tarea
	end	

	context "cuando haya tareas sin terminar" do
		
		scenario "al marcar una tarea como terminada, debe aparecer terminada" do

			terminar_tarea

			expect(page).to have_content("Haz terminado la tarea Tocar guitarra!")

		end	
	
	end

	context "cuando esté terminada una tarea" do

		scenario "dará error si intento terminarla nuevamente" do

			terminar_tarea
			terminar_tarea

			expect(page).to have_content("Esta tarea ya fue terminada!")

		end	
	end	


	def crear_tarea
	
		visit new_task_path

		fill_in "task_name", with: "Tocar guitarra"

		find("#btn-guardar").click 
	
	end

	def terminar_tarea
		
		first_task = "#task_#{Task.first.id}"
		within("#todas") do

			click_on('Terminar')
		
		end

	end

end
