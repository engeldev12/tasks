require 'rails_helper'

RSpec.describe TasksHelper, type: :helper do

	describe "cuando una tarea esté terminada" do
		
		it "su status será 'terminada' " do

			task = double('task', is_done: true)

			expect(helper.show_status(task)[:text]).to eq("terminada")

		end	
	
	end

	describe "cuando una tarea esté sin terminar" do
		
		it "su status será 'pendiente' " do

			task = double('task', is_done: false)

			expect(helper.show_status(task)[:text]).to eq("pendiente")
		end	
	
	end

	describe 'cuando la fecha de creación sea igual al día actual' do

		it 'mostrará hoy' do
			
			dia = Date.new(2019, 5, 20)
			task = double('task', created: dia) 

			expect(helper.show_date(task, dia)).to eq('Hoy')
		
		end	
	end

	describe 'cuando la fecha de creación sea distinta a la actual' do

		it 'mostrará la fecha' do
			
			dia_actual = Date.new(2019, 5, 21)
			fecha_de_creacion = Date.new(2019, 5, 20)
			task = double('task', created: fecha_de_creacion) 

			expect(helper.show_date(task, dia_actual)).to eq("Mon, 20 May 2019")
		
		end	
	end	

end
