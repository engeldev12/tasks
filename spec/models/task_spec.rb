require 'rails_helper'

RSpec.describe Task, type: :model do

	before(:each) do
		@user = User.create(email: "engeljavierpinto@gmail.com", password: "gggg")
	end

	context "cuando creo una tarea nueva" do

		before(:each) do
			@name_task = "Programar"
			@creeted = Date.new 2019, 3, 12
			@task = Task.new name: @name_task, created: @creeted, user: @user
		end	

		it "debe aparecer sin terminar" do	
			
			@task.save 

			expect(@task.is_done).to be false
			expect(Task.undones_of(@user).count).to eq(1)

		end

		it "sin nombre da error al intentar guardarla" do
			
			@task.name = nil			
			
			@task.save

			expect(@task.errors[:name].first).to eq("El nombre es obligatorio!")
	
		end

		it 'va a ser la primera la última tarea creada' do

			ultima = Task.new(name: "Pelear", user: @user) 
			ultima.save

			primera = Task.new(name: "Brincar", user: @user)
			primera.save

			expect(Task.all_desc_order_of(@user)).to eq([primera, ultima])

		end

	end

	context 'cuando termino una tarea' do

		before(:each) do
			@task = Task.new name: "Afeitar", created: DateTime.now, user: @user
		end	

		it "debe aparecer como terminada" do

			@task.done

			expect(@task.is_done).to be true
			expect(Task.dones_of(@user).count).to eq(1)

		end	
		
		it "da error intentar terminarla nuevamente" do

			@task.done
			@task.done
				
			expect(@task.errors[:is_done].first).to eq("Esta tarea ya fue terminada!")
			
		end
		
	end

end
